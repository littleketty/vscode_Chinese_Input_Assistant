# 详细说明

## 中文补全

### 基本原理

简单的说, 本插件的工作原理是:

1. 通过各种方式收集用户可能会输出的中文. 例如当前文件出现过的中文, 代码语义中的中文属性等.
2. 将这些中文转换为其拼写形式, 例如将`你好`转换为`nihao`.
3. 当用户输入这些拼写形式时, 在 vscode 提示框中显示对应的汉字.

### 字典

在上面说到的第二步中, 我们使用了`字典`的方式.

所谓字典是一个`汉字-拼写`的对应关系表. 将中文转换为拼写的方法, 就是一个一个字查表.

特别地, 对于多音字的情况, 我们会同时将所有可能都列出.

例如`恐吓`, 应该读`kong he`, 但我们会将`konghe`和`kongxia`一起加入候选项, 输入任意一个拼写都可以.

这是因为技术限制, 我们暂时没有找到更好的方法.

### 自定义字典

本插件内置了一些字典, 你可以在[这里](https://gitee.com/Program-in-Chinese/vscode_Chinese_Input_Assistant/tree/master/%E5%AD%97%E5%85%B8)看到它们.

你可以仿照这些文件, [编写自己的字典](https://gitee.com/Program-in-Chinese/vscode_Chinese_Input_Assistant/blob/master/%E6%96%87%E6%A1%A3/%E8%87%AA%E5%AE%9A%E4%B9%89%E5%AD%97%E5%85%B8.md).

插件默认启用的是`全拼`, 你可以在这里进行修改:

![](../截图/5.png)
![](../截图/8.png)

例如, 如果你使用五笔, 你可以将`pinyin_simp.dict.txt`注释, 然后打开`wubi86.dict.txt`.

你也可以在这里写完整路径, 使用自己的字典.

另外我们允许同时使用多个字典, 你可以使用`补充字典`等模式自由组合.

注: 我们不太了解五笔, 这个五笔字典应该是五笔`全码`, 如果有任何错误, 或希望加入例如`四码`的输入方式, 请提 issue.

## 本地输入法

我们发现字典模式也可以支持本地输入法, 原理也是类似的,
在用户输入拼写形式时去查表, 即可得到对应的中文候选项.

输入法字典和拼写字典是分开的, 需要分别配置.

插件默认启用的是`全拼`, 你可以在这里进行修改:

![](../截图/6.png)

## 网络输入法

本插件还可以通过百度联想或谷歌网络输入法获取候选词, 原理是将用户输入传给 api, 将 api 返回的结果加入候选项.

## 插件选项

| 选项含义         | 配置项名称                                | 默认值                     | 说明                                                   |
| ---------------- | ----------------------------------------- | -------------------------- | ------------------------------------------------------ |
| 触发补全的字符   | ChineseInputAssistant.triggerChar         | [ "$", ":" ]               | 在键入这些字符后自动弹出补全框                         |
| 禁用补全的语言   | ChineseInputAssistant.disabledLanguage    | [ ".md" ]                  | 在这些语言中禁用补全功能                               |
| 拼写字典         | ChineseInputAssistant.spellDictionaries   | [ "pinyin_simp.dict.txt" ] | 中文补全功能使用的字典                                 |
| 使用 vsc 补全项  | ChineseInputAssistant.useCompletionItem   | "yes"                      | 中文补全结果中包含 vscode 提供的补全项(例如对象的属性) |
| 使用当前文件分词 | ChineseInputAssistant.useFileWord         | "yes"                      | 中文补全结果中包含当前文件中出现的词(例如注释中的词)   |
| 使用本地输入法   | ChineseInputAssistant.useLocInput         | "yes"                      | 使用本地输入法                                         |
| 本地输入法字典   | ChineseInputAssistant.InputDictionaries   | [ "pinyin_simp.dict.txt" ] | 本地输入法功能使用的字典                               |
| 网络输入法       | ChineseInputAssistant.netInputMethod      | "no"                       | 选择使用的网络输入法, "no"为不使用                     |
| 网络输入法代理   | ChineseInputAssistant.netInputMethodProxy | null                       | 网络输入法使用的代理                                   |
